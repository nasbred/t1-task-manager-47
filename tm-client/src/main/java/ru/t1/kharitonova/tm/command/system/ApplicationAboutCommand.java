package ru.t1.kharitonova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.kharitonova.tm.dto.response.system.ServerAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getServiceLocator().getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display developer info.";
    }

}
