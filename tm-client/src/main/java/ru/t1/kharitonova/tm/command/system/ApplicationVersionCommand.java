package ru.t1.kharitonova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.kharitonova.tm.dto.response.system.ServerVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionResponse response = getServiceLocator().getSystemEndpoint().getVersion(new ServerVersionRequest());
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display program version.";
    }

}
