package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.user.UserProfileRequest;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "View current user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO userDTO = getAuthEndpoint().profile(request).getUserDTO();
        if (userDTO == null) return;
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID : " + userDTO.getId());
        System.out.println("LOGIN : " + userDTO.getLogin());
        System.out.println("FIRST NAME : " + userDTO.getFirstName());
        System.out.println("MIDDLE NAME : " + userDTO.getMiddleName());
        System.out.println("LAST NAME : " + userDTO.getLastName());
        System.out.println("EMAIL : " + userDTO.getEmail());
        System.out.println("ROLE : " + userDTO.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
