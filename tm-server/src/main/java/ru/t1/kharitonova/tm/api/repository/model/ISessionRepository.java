package ru.t1.kharitonova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    void removeOneById(@NotNull String id);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    @Nullable
    Session findOneByUserId(@NotNull String userId);

    long getSize();

}
