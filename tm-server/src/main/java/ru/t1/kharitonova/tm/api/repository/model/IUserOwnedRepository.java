package ru.t1.kharitonova.tm.api.repository.model;

import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends IAbstractRepository<M> {

}
