package ru.t1.kharitonova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    List<User> findAll();

    long getSize();

    void setPassword(@Nullable String id, @NotNull String password_hash);

    void updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    void lockUserById(@Nullable String id);

    void unLockUserById(@Nullable String id);

    void removeAll();

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

}
