package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    SessionDTO validateToken(@Nullable String token);

    void invalidate(SessionDTO sessionDTO);

}
