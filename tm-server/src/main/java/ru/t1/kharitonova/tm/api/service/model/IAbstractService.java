package ru.t1.kharitonova.tm.api.service.model;

import ru.t1.kharitonova.tm.api.repository.model.IAbstractRepository;
import ru.t1.kharitonova.tm.model.AbstractModel;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {
}
