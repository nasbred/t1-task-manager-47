package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.ICalculatorEndpoint;
import ru.t1.kharitonova.tm.api.service.IServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.kharitonova.tm.api.endpoint.ICalculatorEndpoint")
public final class CalculatorEndpoint extends AbstractEndpoint implements ICalculatorEndpoint {

    public CalculatorEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

}
