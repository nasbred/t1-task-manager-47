package ru.t1.kharitonova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.kharitonova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IAbstractDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
