package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
