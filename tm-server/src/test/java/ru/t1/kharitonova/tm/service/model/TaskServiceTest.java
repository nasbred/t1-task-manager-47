package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.api.service.model.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.service.ConnectionService;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Category(ServerCategory.class)
public class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        if (userService.findByLogin("admin") == null) {
            @NotNull final User userAdmin = new User();
            userAdmin.setLogin("admin");
            userAdmin.setPasswordHash("admin");
            userAdmin.setEmail("admin@test.ru");
            userAdmin.setRole(Role.ADMIN);
            userService.add(userAdmin);
        }
        if (userService.findByLogin("test") == null) {
            @NotNull final User userTest = new User();
            userTest.setLogin("test");
            userTest.setPasswordHash("test");
            userTest.setEmail("test@test.ru");
            userTest.setRole(Role.USUAL);
            userService.add(userTest);
        }
        @Nullable User userAdminBase = userService.findByLogin("admin");
        @Nullable User userTestBase = userService.findByLogin("test");
        userList.add(userAdminBase);
        userList.add(userTestBase);

        projectList.add(new Project(userAdminBase, "Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(userAdminBase, "Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userTestBase, "Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userTestBase, "Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(userAdminBase, "Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0)));
        taskList.add(new Task(userAdminBase, "Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1)));
        taskList.add(new Task(userTestBase, "Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2)));
        taskList.add(new Task(userTestBase, "Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        taskService.removeAll();
        projectService.removeAll();
        for (@NotNull final Project projectDTO : projectList) projectService.add(projectDTO);
        for (@NotNull final Task taskDTO : taskList) taskService.add(taskDTO);
    }

    @After
    public void clear() {
        taskService.removeAll();
        projectService.removeAll();
        taskList.clear();
        projectList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Task task = new Task(
                userList.get(0),
                "Test Task",
                "Test Add Task",
                Status.NOT_STARTED,
                null);
        taskService.add(task);
        Assert.assertTrue(taskService.existsById(task.getUser().getId(), task.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<Task> tasks = new ArrayList<>();
        final long taskSize = taskService.getSize();
        for (int i = 0; i < taskSize; i++) {
            tasks.add(new Task(
                    userList.get(0),
                    "Task Test Add " + i,
                    "Task Add Desc " + i,
                    Status.NOT_STARTED,
                    projectList.get(0)));
        }
        taskService.addAll(tasks);
        Assert.assertEquals(taskSize * 2, taskService.getSize());

        taskService.removeAll(tasks);
    }

    @Test
    public void testCreate() {
        for (@NotNull final User user : userList) {
            @NotNull final String user_Id = user.getId();
            System.out.println(user_Id);
            @NotNull final String name = "Task" + user.getLogin();
            @NotNull final String description = "Description_" + user.getLogin();
            @NotNull final Task task = taskService.create(user_Id, name, description);
            Assert.assertNotNull(task);
            taskService.remove(task);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(taskService.create(
                null,
                "Test Task",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(taskService.create(
                userList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUser().getId();
            @NotNull final String id = task.getId();
            taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
            @Nullable final Task actualTask = taskService.findOneById(userId, id);
            Assert.assertNotNull(actualTask);
            @NotNull final Status actualStatus = actualTask.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> userTasks = taskService.findAll(userId);
            for (int i = 0; i < userTasks.size(); i++) {
                @Nullable final Task task = taskService.findOneByIndex(userId, i);
                Assert.assertNotNull(task);
                taskService.changeTaskStatusByIndex(userId, i, Status.IN_PROGRESS);
                @NotNull final Status status = taskService.findOneById(task.getUser().getId(), task.getId()).getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> userTasks = taskService.findAll(userId);
            taskService.changeTaskStatusByIndex(userId, userTasks.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @NotNull final String user_id = task.getUser().getId();
            Assert.assertTrue(taskService.existsById(user_id, id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(taskService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskService.getSize(), tasks.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> actualTasks = taskService.findAll(userId);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(actualTasks.size(), expectedTasks.size());
            for (int i = 0; i < actualTasks.size(); i++) {
                Assert.assertEquals(actualTasks.get(i).getId(), expectedTasks.get(i).getId());
            }
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @NotNull final String user_id = task.getUser().getId();
            Assert.assertEquals(task.getId(), taskService.findOneById(user_id, id).getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @NotNull final String user_id = "Other_user_id";
        @Nullable final Task task = taskService.findOneById(user_id, id);
        Assert.assertNull(task);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @Nullable final String userId = task.getUser().getId();
            Assert.assertEquals(task.getId(), taskService.findOneById(userId, id).getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Task task = taskService.findOneById(userId, id);
        Assert.assertNull(task);
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                Assert.assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final Task actualTask = taskService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualTask);
                @Nullable final Task expectedTask = tasks.get(i);
                Assert.assertEquals(expectedTask.getId(), actualTask.getId());
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String user_id = "Other_User_Id";
        final int index = taskList.size() + 1;
        Assert.assertNull(taskService.findOneByIndex(user_id, index));
    }

    @Test
    public void testGetSize() {
        final long taskSize = taskService.getSize();
        Assert.assertEquals(taskList.size(), taskSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String user_id = user.getId();
            final long actualProjectSize = taskService.getSize(user_id);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> user_id.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            final int expectedTaskSize = expectedTasks.size();
            Assert.assertEquals(expectedTaskSize, actualProjectSize);
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final Task task = taskList.get(i);
            taskService.remove(task);
            Assert.assertEquals(taskService.getSize(), taskList.size() - i - 1);
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @NotNull final String user_id = task.getUser().getId();
            taskService.removeOneById(user_id, id);
            Assert.assertNull(taskService.findOneById(user_id, id));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String user_id = "Other_User_Id";
        taskService.removeOneById(user_id, id);
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String user_id = user.getId();
            @Nullable List<Task> tasksBcp = taskService.findAll(user_id);
            taskService.removeAllByUserId(user_id);
            Assert.assertEquals(0, taskService.getSize(user_id));
            taskService.addAll(tasksBcp);
        }
    }

    @Test
    public void testRemoveAllByUserIdNegative() {
        long expected_size = taskService.getSize();
        taskService.removeAllByUserId("Other_user_id");
        Assert.assertEquals(expected_size, taskService.getSize());
    }

    @Test
    public void testUpdateByIdPositive() {
        @Nullable List<Task> tasks = taskService.findAll();
        if (tasks == null) {
            taskService.addAll(taskList);
            tasks = taskService.findAll();
        }
        for (@NotNull final Task task : tasks) {
            @Nullable final String userId = task.getUser().getId();
            @NotNull final String id = task.getId();
            @NotNull final String name = "Task Test Update";
            @NotNull final String description = "Description Test Update";
            @Nullable final Task actualTask = taskService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualTask.getName());
            Assert.assertEquals(description, actualTask.getDescription());

            taskService.remove(task);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = "Other_id";
        @NotNull final String name = "Task Test Update " + id;
        @NotNull final String description = "Description Test Update " + id;
        Assert.assertNotNull(taskService.updateById(userId, id, name, description));
    }

}
